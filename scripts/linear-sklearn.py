'''
  This software is licensed under the terms of the MIT license
  See COPYING for further information.
  ---
  Copyright (c) 2019, A. Vorobyev.
'''

import argparse
import numpy
import multiprocessing
import pandas
from sklearn.linear_model import LinearRegression
from sklearn import model_selection

cmdparser = argparse.ArgumentParser()
cmdparser.add_argument("dataset", help="name of the file with data set", type=str)
args = cmdparser.parse_args()

data = pandas.read_csv(args.dataset, sep=',', header=None)

classColumn = data.shape[1] - 1
classes = data[classColumn]
objFeatures = data[list(range(0, classColumn))]

kf = model_selection.KFold(n_splits=8192, shuffle=True)

lr = LinearRegression(normalize=False, n_jobs=multiprocessing.cpu_count())

scores = model_selection.cross_val_score(lr, objFeatures, classes, cv=kf, scoring='neg_mean_squared_error')
for i in range(0, scores.shape[0]):
  scores[i] = scores[i]

print(abs(numpy.mean(scores)))
