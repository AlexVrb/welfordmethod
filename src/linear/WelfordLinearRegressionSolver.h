/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#ifndef WELFORDLINEARREGRESSIONSOLVER_H
#define WELFORDLINEARREGRESSIONSOLVER_H

#include <numeric>

#include "CommonDefines.h"
#include "KahanAccumulator.h"

class LinearModel {
  public:
    LinearModel(const std::vector<double>& aWeights, const double aFreevar)
        : weights(aWeights), freevar(aFreevar)
    {
    }

    LinearModel()
    {
    }

    double solve(const std::vector<double>& features) const
    {
        return std::inner_product(weights.begin(), weights.end(),
                                  features.begin(), freevar);
    }

    void setModel(const std::vector<double>& aWeights, const double aFreevar)
    {
        weights = aWeights;
        freevar = aFreevar;
    }

    void getParameters(std::vector<double>& vec, double& free) const
    {
        vec = weights;
        free = freevar;
    }

  private:
    std::vector<double> weights;
    double freevar;
};

class WelfordLinearRegressionSolver {
  public:
    int add(const std::vector<double>& object,
            const double goal,
            const double weight = 1.);

    int solve(LinearModel&) const;

  private:
    KahanAccumulator m_goalsMean = 0.;
    KahanAccumulator m_goalsDeviation = 0.;
    KahanAccumulator m_weightsSum = 0.;
    std::vector<double> m_featureMeans;
    std::vector<double> m_featureDeviationFromLastMean;
    std::vector<double> m_featureDeviationFromNew;
    matrix<double> m_aMatrix;
    std::vector<double> m_bVector;
};

#endif
