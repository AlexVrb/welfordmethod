/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#ifndef CROSSVALIDATION_H
#define CROSSVALIDATION_H

#include "CommonDefines.h"
#include "WelfordLinearRegressionSolver.h"

class CrossValidation {
  public:
    CrossValidation();

    int validate(const matrix<double>& dataSet, double& validationResult) const;

  private:
    double rsquare(const std::vector<double>& y,
                   const std::vector<double>& f) const;
    double mse(const std::vector<double>& y,
               const std::vector<double>& f) const;
};

#endif
