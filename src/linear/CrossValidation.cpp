/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#include <algorithm>
#include <iostream>
#include <numeric>
#include <random>

#include "CrossValidation.h"
#include "WelfordLinearRegressionSolver.h"

CrossValidation::CrossValidation()
{
}

int CrossValidation::validate(const matrix<double>& dataSet,
                              double& validationResult) const
{
    std::vector<double> errors;

    for(size_t i = 0; i < dataSet.size(); ++i) {
        matrix<double> tempDataSet = dataSet;

        std::vector<double> testObj = tempDataSet[i];
        tempDataSet.erase(tempDataSet.begin() + i);

        WelfordLinearRegressionSolver solver;

        if(dataSet.size() - tempDataSet.size() != 1) {
            std::cerr << "Size isn't lesser by 1?\n";
        }

        // обучение на почти всей выборке
        for(size_t j = 0; j < tempDataSet.size(); ++j) {
            double dclass = tempDataSet[j][tempDataSet[j].size() - 1];
            tempDataSet[j].pop_back();
            solver.add(tempDataSet[j], dclass);
        }

        LinearModel solution;
        int result = solver.solve(solution);
        if(result) {
            std::cerr << "Something went wrong!\n";
            return result;
        }

        // тест
        const double dclass = testObj[testObj.size() - 1];
        testObj.pop_back();
        if(testObj.size() != tempDataSet[0].size()) {
            std::cerr << "Size isn't lesser by 1?\n";
        }

        const double pred = solution.solve(testObj);

        errors.push_back(pow(dclass - pred, 2));
    }

    double sum = 0.;
    for(size_t i = 0; i < errors.size(); ++i) {
        sum += errors[i] / errors.size();
    }

    validationResult = sum;

    return 0;
}

double CrossValidation::rsquare(const std::vector<double>& y,
                                const std::vector<double>& f) const
{
    const double ymean = std::accumulate(y.begin(), y.end(), 0.) / y.size();

    double sum1 = 0.;
    for(size_t i = 0; i < y.size(); ++i) {
        sum1 += pow(y[i] - f[i], 2);
    }

    double sum2 = 0.;
    for(size_t i = 0; i < y.size(); ++i) {
        sum2 += pow(y[i] - ymean, 2);
    }

    return (1. - sum1 / sum2);
}

double CrossValidation::mse(const std::vector<double>& y,
                            const std::vector<double>& f) const
{
    double sumError = 0.;
    for(size_t i = 0; i < y.size(); ++i) {
        sumError += pow(y[i] - f[i], 2) / y.size();
    }

    return sumError;
}
