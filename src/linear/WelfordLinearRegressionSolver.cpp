/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#include <iostream>

#define EIGEN_VECTORIZE_SSE
#include <Eigen/LU>

#include "WelfordLinearRegressionSolver.h"

int WelfordLinearRegressionSolver::add(const std::vector<double>& object,
                                       const double goal,
                                       const double weight)
{
    const size_t featuresAmount = object.size();

    // начальное распределение памяти
    if(m_featureMeans.empty()) {
        m_featureMeans.resize(featuresAmount);
        m_featureDeviationFromLastMean.resize(featuresAmount);
        m_featureDeviationFromNew.resize(featuresAmount);
        m_bVector.resize(featuresAmount);

        m_aMatrix.resize(featuresAmount);
        for(size_t i = 0; i < m_aMatrix.size(); ++i) {
            m_aMatrix[i].resize(featuresAmount);
        }
    }

    m_weightsSum += weight;
    if(m_weightsSum == 0.) {
        return -1;
    }

    // шаг по методу Уэлфорда
    for(size_t i = 0; i < featuresAmount; ++i) {
        double feature = object[i];

        m_featureDeviationFromLastMean[i] =
            weight * (feature - m_featureMeans[i]);
        m_featureMeans[i] +=
            weight * (feature - m_featureMeans[i]) / m_weightsSum;
        m_featureDeviationFromNew[i] = feature - m_featureMeans[i];
    }

    // пересчет коэффициентов СЛАУ
    for(size_t i = 0; i < m_aMatrix.size(); ++i) {
        for(size_t j = 0; j < m_aMatrix[i].size(); ++j) {
            m_aMatrix[i][j] += m_featureDeviationFromLastMean[i] *
                               m_featureDeviationFromNew[j];
        }
    }

    for(size_t i = 0; i < m_bVector.size(); ++i) {
        m_bVector[i] =
            weight * (goal - m_goalsMean) * m_featureDeviationFromNew[i];
    }

    const double oldGoalsMean = m_goalsMean;
    m_goalsMean += weight * (goal - m_goalsMean) / m_weightsSum;
    m_goalsDeviation += weight * (goal - oldGoalsMean) * (goal - m_goalsMean);

    return 0;
}

int WelfordLinearRegressionSolver::solve(LinearModel& solution) const
{
    std::vector<double> weights;
    KahanAccumulator freevar(0.);

    if(m_aMatrix.empty() || m_bVector.empty()) {
        return -1;
    }

    // решение СЛАУ
    Eigen::MatrixXd A(m_aMatrix.size(), m_aMatrix[0].size());
    for(size_t i = 0; i < m_aMatrix.size(); ++i) {
        for(size_t j = 0; j < m_aMatrix[i].size(); ++j) {
            A(i, j) = m_aMatrix[i][j];
        }
    }

    Eigen::VectorXd b(m_bVector.size());
    for(size_t i = 0; i < m_bVector.size(); ++i) {
        b(i) = m_bVector[i];
    }

    b.stableNormalize();

    Eigen::VectorXd x = A.lu().solve(b);

    for(Eigen::Index i = 0; i < x.rows(); ++i) {
        weights.push_back(x[i]);
    }

    // привести к нецентрированному ответу
    for(size_t i = 0; i < weights.size(); ++i) {
        freevar += -m_featureMeans[i] * weights[i];
    }

    solution.setModel(weights, freevar);

    return 0;
}
