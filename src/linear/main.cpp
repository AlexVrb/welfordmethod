/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include "CrossValidation.h"

const std::string varDS = "dataset";

int readDataSet(const std::string& fileName, matrix<double>& dataSet)
{
    std::ifstream inputFile;
    inputFile.open(fileName, std::ifstream::in);
    if(!inputFile) {
        return -1;
    }

    std::string temp;
    while(getline(inputFile, temp)) {
        std::vector<double> object;

        std::vector<std::string> splitValues;
        boost::split(splitValues, temp, boost::is_any_of("\t,\n"));
        for(size_t i = 0; i < splitValues.size() /*- 1*/; ++i) {
            double value = std::stod(splitValues[i]);
            // std::cout << value << '\n';
            object.push_back(value);
        }

        dataSet.push_back(object);
    }

    return 0;
}

matrix<double> scale(const matrix<double>& dataset)
{
    matrix<double> res(dataset.size());

    // выборочные средние
    std::vector<double> means(dataset.size(), 0);
    for(unsigned int i = 0; i < dataset.size(); ++i) {
        for(unsigned int j = 0; j < dataset[i].size(); ++j) {
            means[j] += dataset[i][j] / static_cast<double>(dataset.size());
        }
    }

    // выборочные дисперсии
    std::vector<double> disps(dataset.size(), 0);
    for(unsigned int i = 0; i < dataset.size(); ++i) {
        for(unsigned int j = 0; j < dataset[i].size(); ++j) {
            disps[j] += pow(dataset[i][j] - means[j], 2) /
                        static_cast<double>(dataset.size());
        }
    }

    for(unsigned int i = 0; i < res.size(); ++i) {
        res[i].resize(dataset[i].size());

        for(unsigned int j = 0; j < res[i].size(); ++j) {
            res[i][j] = (dataset[i][j] - means[j]) / sqrt(disps[j]);
        }
    }

    return res;
}

int main(int argc, char** argv)
{
    boost::program_options::options_description desc("Options");
    desc.add_options()(varDS.c_str(),
                       boost::program_options::value<std::string>(),
                       "name of file with data set");

    boost::program_options::variables_map vmArgs;
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, desc), vmArgs);
    boost::program_options::notify(vmArgs);

    // read dataSet
    std::string fileName;
    if(vmArgs.count(varDS)) {
        fileName = vmArgs[varDS].as<std::string>();
    }
    else {
        std::cerr << "No dataset provided!\n";
        return -1;
    }

    matrix<double> data;
    int result = readDataSet(fileName, data);
    if(result) {
        std::cerr << "Can't read dataset\n";
        return result;
    }

    data = scale(data);

    // output

    CrossValidation validator;
    double res = 0.;

    // cross-validation
    auto clStart = std::chrono::high_resolution_clock::now();
    result = validator.validate(data, res);
    auto clEnd = std::chrono::high_resolution_clock::now();

    std::cout << "Result: " << std::setprecision(20) << res << ", time: "
              << std::chrono::duration_cast<std::chrono::duration<double>>(
                     clEnd - clStart)
                     .count()
              << '\n';

    return 0;
}
