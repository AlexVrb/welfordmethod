/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#include <fstream>
#include <iomanip>
#include <iostream>

#include <boost/algorithm/string.hpp>

#include "CommonDefines.h"

int readDataSet(const std::string& fileName, matrix<double>& dataSet)
{
    std::ifstream inputFile;
    inputFile.open(fileName, std::ifstream::in);
    if(!inputFile) {
        return -1;
    }

    std::string temp;
    while(getline(inputFile, temp)) {
        std::vector<double> object;

        std::vector<std::string> splitValues;
        boost::split(splitValues, temp, boost::is_any_of("\t,\n"));
        for(size_t i = 0; i < splitValues.size(); ++i) {
            double value = std::stod(splitValues[i]);
            object.push_back(value);
        }

        dataSet.push_back(object);
    }

    return 0;
}

matrix<double> noiseIt(const matrix<double>& mat)
{
    matrix<double> res(mat.size());

    for(size_t i = 0; i < res.size(); ++i) {
        res[i].resize(mat[i].size());

        // change only one feature to 1
        /*res[i][0] = 1.;
        for(size_t j = 1; j < res[i].size(); ++j)
        {
            res[i][j] = mat[i][j];
        }*/
        for(size_t j = 0; j < res[i].size() - 1; ++j) {
            res[i][j] = 1.;
        }
        res[i][res[i].size() - 1] = mat[i][mat[i].size() - 1];
    }

    return res;
}

int writeDataSet(const std::string& fileName, const matrix<double>& dataSet)
{
    std::ofstream outputFile;
    outputFile.open(fileName, std::ifstream::out);
    if(!outputFile) {
        return -1;
    }

    for(size_t i = 0; i < dataSet.size(); ++i) {
        for(size_t j = 0; j < dataSet[i].size() - 1; ++j) {
            outputFile << std::setprecision(6) << dataSet[i][j] << ',';
        }

        outputFile << std::setprecision(6) << dataSet[i][dataSet[i].size() - 1]
                   << '\n';
    }

    return 0;
}

int main(int argc, char** argv)
{
    // read dataSet
    matrix<double> data;
    int result = readDataSet("train.txt", data);
    if(result) {
        std::cerr << "Can't read dataset\n";
        return result;
    }

    matrix<double> noised = noiseIt(data);

    result = writeDataSet("noisedTrain.txt", noised);
    if(result) {
        std::cerr << "Can't write noised dataset\n";
        return result;
    }

    return 0;
}
