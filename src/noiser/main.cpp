/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#include <fstream>
#include <iomanip>
#include <iostream>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include "CommonDefines.h"

const std::string optFactor = "factor";
const std::string optOffset = "offset";

int readDataSet(const std::string& fileName, matrix<double>& dataSet)
{
    std::ifstream inputFile;
    inputFile.open(fileName, std::ifstream::in);
    if(!inputFile) {
        return -1;
    }

    std::string temp;
    while(getline(inputFile, temp)) {
        std::vector<double> object;

        std::vector<std::string> splitValues;
        boost::split(splitValues, temp, boost::is_any_of("\t,\n"));
        for(size_t i = 0; i < splitValues.size(); ++i) {
            double value = std::stod(splitValues[i]);
            object.push_back(value);
        }

        dataSet.push_back(object);
    }

    return 0;
}

matrix<double>
noiseIt(const matrix<double>& mat, const double factor, const double offset)
{
    matrix<double> res(mat.size());

    for(size_t i = 0; i < res.size(); ++i) {
        res[i].resize(mat[i].size());

        for(size_t j = 0; j < res[i].size() - 1; ++j) {
            res[i][j] = factor * mat[i][j] + offset;
        }

        res[i][res[i].size() - 1] = mat[i][mat[i].size() - 1];
    }

    return res;
}

int writeDataSet(const std::string& fileName, const matrix<double>& dataSet)
{
    std::ofstream outputFile;
    outputFile.open(fileName, std::ifstream::out);
    if(!outputFile) {
        return -1;
    }

    for(unsigned int i = 0; i < dataSet.size(); ++i) {
        for(unsigned int j = 0; j < dataSet[i].size() - 1; ++j) {
            outputFile << std::setprecision(6) << dataSet[i][j] << ',';
        }

        outputFile << std::setprecision(6) << dataSet[i][dataSet[i].size() - 1]
                   << '\n';
    }

    return 0;
}

int main(int argc, char** argv)
{
    boost::program_options::options_description desc("Options");
    desc.add_options()(optFactor.c_str(),
                       boost::program_options::value<double>(), "noise factor")(
        optOffset.c_str(), boost::program_options::value<double>(),
        "noise offset");

    boost::program_options::variables_map vmArgs;
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, desc), vmArgs);
    boost::program_options::notify(vmArgs);

    double factor;
    if(vmArgs.count(optFactor)) {
        factor = vmArgs[optFactor].as<double>();
    }
    else {
        std::cerr << "No factor provided!\n";
        return -1;
    }

    double offset;
    if(vmArgs.count(optOffset)) {
        offset = vmArgs[optOffset].as<double>();
    }
    else {
        std::cerr << "No offset provided!\n";
        return -1;
    }

    // read dataSet
    matrix<double> data;
    int result = readDataSet("train.txt", data);
    if(result) {
        std::cerr << "Can't read dataset\n";
        return result;
    }

    matrix<double> noised = noiseIt(data, factor, offset);

    result = writeDataSet("noisedTrain.txt", noised);
    if(result) {
        std::cerr << "Can't write noised dataset\n";
        return result;
    }

    return 0;
}
