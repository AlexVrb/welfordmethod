/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#include <fstream>
#include <iostream>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <mlpack/core/cv/k_fold_cv.hpp>
#include <mlpack/core/cv/metrics/mse.hpp>
#include <mlpack/methods/linear_regression/linear_regression.hpp>

const std::string varDS = "dataset";

int readDataSet(const std::string& fileName,
                arma::mat& objs,
                arma::rowvec& classes)
{
    std::ifstream inputFile;
    inputFile.open(fileName, std::ifstream::in);
    if(!inputFile) {
        return -1;
    }

    std::string temp;
    unsigned int j = 0;
    while(getline(inputFile, temp)) {

        std::vector<std::string> splitValues;
        boost::split(splitValues, temp, boost::is_any_of("\t,\n"));
        for(size_t i = 0; i < splitValues.size() - 1; ++i) {
            double value = std::stod(splitValues[i]);

            objs(j, i) = value;
        }

        classes(j) = std::stod(splitValues[splitValues.size() - 1]);
        ++j;
    }

    return 0;
}

int main(int argc, char** argv)
{
    boost::program_options::options_description desc("Options");
    desc.add_options()(varDS.c_str(),
                       boost::program_options::value<std::string>(),
                       "name of file with data set");

    boost::program_options::variables_map vmArgs;
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, desc), vmArgs);
    boost::program_options::notify(vmArgs);

    // read dataSet
    std::string fileName;
    if(vmArgs.count(varDS)) {
        fileName = vmArgs[varDS].as<std::string>();
    }
    else {
        std::cerr << "No dataset provided!\n";
        return -1;
    }

    arma::mat data(8192, 8);
    arma::rowvec classes(8192);

    int result = readDataSet(fileName, data, classes);
    if(result) {
        std::cerr << "Error reading file\n";
        return result;
    }

    double mse = 0.;
    auto clStart = std::chrono::high_resolution_clock::now();
    try {
        mlpack::cv::KFoldCV<mlpack::regression::LinearRegression,
                            mlpack::cv::MSE>
            kf(10, data.t(), classes);
        mse = kf.Evaluate(0., true);
    }
    catch(std::exception& exc) {
        std::cerr << "Caught exception: " << exc.what() << '\n';
        return -1;
    }
    auto clEnd = std::chrono::high_resolution_clock::now();

    std::cout << "Result: " << mse << ", time: "
              << std::chrono::duration_cast<std::chrono::duration<double>>(
                     clEnd - clStart)
                     .count()
              << '\n';

    return 0;
}
