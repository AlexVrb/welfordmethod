/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#ifndef KAHANACCUMULATOR_H
#define KAHANACCUMULATOR_H

class KahanAccumulator {
  public:
    KahanAccumulator(const double value) : m_sum(value), m_error(0.)
    {
    }

    KahanAccumulator& operator+=(const double value)
    {
        const double corrValue = value - m_error;
        const double newSum = m_sum + corrValue;
        m_error = (newSum - m_sum) - corrValue;
        m_sum = newSum;
        return *this;
    }

    KahanAccumulator& operator=(const double value)
    {
        m_sum = value;
        m_error = 0.;
        return *this;
    }

    KahanAccumulator& operator+=(const KahanAccumulator& other)
    {
        return *this += static_cast<double>(other);
    }

    operator double() const
    {
        return m_sum + m_error;
    }

  private:
    double m_sum = 0.;
    double m_error = 0.;
};

#endif
