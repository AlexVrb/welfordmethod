/*
 *  This software is licensed under the terms of the MIT license
 *  See COPYING for further information.
 *  ---
 *  Copyright (c) 2019, A. Vorobyev.
 */

#ifndef COMMONDEFINES_H
#define COMMONDEFINES_H

#include <vector>

template <typename T> using matrix = std::vector<std::vector<T>>;

#endif
