echo "Welford normal:"
./linear --dataset train.txt
echo

echo "MLPack normal:"
./mlpack_test --dataset train.txt
echo

echo "SK-Learn normal:"
python3 scripts/linear-sklearn.py train.txt
echo

echo "Welford noised:"
./linear --dataset noisedTrain.txt
echo

echo "MLPack noised:"
./mlpack_test --dataset noisedTrain.txt
echo

echo "SK-Learn noised:"
python3 scripts/linear-sklearn.py noisedTrain.txt
